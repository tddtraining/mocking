## How to Contribute
This is a quick and simple guide on how work should be contributed to this project. 
Note that any branches found to violate the principles laid out in this document will not be merged.

### Basic Steps
1. Fork the project 
2. Create your feature branch (`git checkout -b feat/AddFoobar`)
3. Commit your changes (`git commit -am 'Added some foobar'`)
4. Push to the branch (`git push origin feat/AddFoobar`)
5. Create a new merge request when work is complete

### Branch Strategy
This project follows a basic Git Flow methodology when it comes to branching.

When starting a new task, work should go into a new feature branch based on the most recent commit 
in the **develop** branch. 
New branches can be based on existing feature branches if urgently required, 
but should be rebased onto develop as soon as possible.

If the task you are working on is based on a JIRA item, 
prefix the name of the branch with that items JIRA issue number (Eg: feat/HOBC-42-AddFooBar)

Every branch apart from **develop** and **master**
must be preceded with a prefix depending on the type of work:

* **feat/** - Any new features and changes or fixes to existing features
* **release/** - For release candidates. These branches should be based on the **develop** 
  branch only. Such branches typically only contains changes to the project version details, 
  but may change other urgent details if they are interfering with the release pipeline. 
  These are typically the only branches that can be merged into **master**, except for hotfixes.
* **hotfix/** - High priority fixes that must go into a release copy *immediately*, 
  these branches can be merged directly into master

Typical development work should use **feat/**.

### Committing Work
All code submitted to the project must be constructed using test driven development. 
Tests should be derived from the description and acceptance criteria in the relevant JIRA task.

When committing work, try to ensure commits are small and manageable and well described. If a commit cannot be 
accurately summarised in a single sentence, it may be too big and should be broken down.

### Merging a branch
**NOTE:** Before opening a merge request, remember to update the CHANGELOG.md file. 
The Gitlab pipeline will fail until this has been done! 
You may also want to consider updating the project version, but if doing so, 
keep an eye on the current version number in **develop**.

When you believe you have completed all relevant work within a particular feature branch, 
open up a merge request in GitLab to merge your feature branch into the **develop** branch.
However, before opening a merge request, ensure the code compiles and passes all unit tests 
by running `maven clean install` in the root directory.

Once you've opened a merge request, get in touch with another developer to review and the change.
Be prepared to receive feedback on your work and discuss any issues that arise during the review.
If the reviewer is happy with your work, they will approve the merge request for you.
