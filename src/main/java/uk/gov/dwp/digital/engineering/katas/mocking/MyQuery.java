package uk.gov.dwp.digital.engineering.katas.mocking;

public class MyQuery {

  private MyService myService;

  public MyQuery(MyService service) {
    myService = service;
  }

  public boolean testExists(String target) {
    return myService.queryCount(target) > 0;
  }
}
