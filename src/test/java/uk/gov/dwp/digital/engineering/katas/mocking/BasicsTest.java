package uk.gov.dwp.digital.engineering.katas.mocking;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

/**
 * The Basics example illustrates a practical call from a class under test to a dependency that needs to be mocked.
 *
 * In this example Mockito is used to create a simple stub that is then expanded in each test with mocked behaviour. The
 * mocked object inherently acts as a spy to permit verification of the dependency calls.
 *
 * Mock behaviour uses Mockito's fluent API when-then syntax to describe what happens when the mocked object is called with
 * a specific given set of arguments.
 *
 * The Mockito verify method is less obvious but automatically provides methods to match the mocked object enabling like-for-
 * like syntax.
 */
public class BasicsTest {

  /*
    Inform mockito to use the @Mock annotation to generate junit mock objects - without this it is necessary to use the inline
    mock() method to generate the mock in the test preparation code. In this example it doesn't illustrate any obvious
    advantage to the annotation but in a more complex scenario the annotation becomes a very efficient approach.
   */
  @Rule
  public MockitoRule mockitoRule = MockitoJUnit.rule();

  /*
    Create a mock of the MyService object
   */
  @Mock
  MyService serviceMock;

  @Test
  public void whenRecordsAreNotPresentTestExistsShouldReturnFalse() {
    String absentTarget = "absent";
    // create a mock behaviour for the mocked object - return 0 when the target value "absent" is given
    // the mockito fluent api is used to describe the behaviour in a when-then syntax
    when(serviceMock.queryCount(absentTarget)).thenReturn(0);
    MyQuery myQuery = new MyQuery(serviceMock);
    Assert.assertThat("Query response should be false", myQuery.testExists(absentTarget), is(false));
    // use mockito verify to confirm that the dependency call to MyService was made
    verify(serviceMock).queryCount(absentTarget);
  }

  @Test
  public void whenRecordsArePresentTestExistsShouldReturnTrue() {
    String presentTarget = "present";
    // create a mock behaviour for the mocked object - return 1 when the target value "present" is given
    when(serviceMock.queryCount(presentTarget)).thenReturn(1);
    MyQuery myQuery = new MyQuery(serviceMock);
    Assert.assertThat("Query response should be true", myQuery.testExists(presentTarget), is(true));
    // use mockito verify to confirm that the dependency call to MyService was made
    verify(serviceMock).queryCount(presentTarget);
  }
}
