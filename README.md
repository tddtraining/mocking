# Mocking Examples

## Testing In Isolation

These worked examples are an accompaniment to the Testing In Isolation lesson. 
Please make sure you've read through the lesson to clarify the terms used 
here.

## Mockito

The examples here use Java 1.8 and Mockito to illustrate the different mock 
concepts.

The project is built through Maven and while it can be built from the 
command line it is best to use a high quality IDE (like IntelliJ IDEA) to 
operate the tests so the different examples can be examined in isolation.
